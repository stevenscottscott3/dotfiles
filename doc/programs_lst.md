# Introduction
In this document you will find a concrete list of all the programs I use along with helpful links and resources.

## Core Operating System
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| linux      |
| runit      |

## Core Utilities
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|

## Audio Server Applications
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| pulseaudio |
| pactlt     |

## Development Utilities
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|

## Networking Utilities
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|

## Privacy and Security Utilities
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|

## Graphical User Interface
| Name        | Description                                                                                | Configuration File                                                                                                | Documentation                                                            | Fork Homepage                                                           | Project Homepage                                       |
| :-----------| :------------------------------------------------------------------------------------------| :-----------------------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-----------------------------------------------------------------------| :------------------------------------------------------|
| dwm         | A dynamic window manager for X.                                                            | [config.def.h](https://gitlab.com/FOSSilized_Daemon/dwm/-/blob/master/config.def.h)                               | [README]()                                                               | [FOSSilized_Daemon's dwm](https://gitlab.com/FOSSilized_Daemon/dwm)     | [dwm](https://dwm.suckless.org/)                       |
| st          | A simple terminal implementation for X.                                                    | [ADDME](OOOO)                                                                                                     | [README]()                                                               | [FOSSilized_Daemon's st](OOOO)                                          | [st](https://st.suckless.org/)                         |
| slock       | Simple X display locker.                                                                   | [ADDME](OOOO)                                                                                                     | [README]()                                                               | [FOSSilized_Daemon's slock](OOOO)                                       | [slock](https://tools.suckless.org/slock/)             |
| xwallpaper  | A utility that allows you to set image files as your X wallpaper.                          | N/A                                                                                                               | [README](https://github.com/stoeckmann/xwallpaper/blob/master/README.md) | N/A                                                                     | [xwallpaper](https://github.com/stoeckmann/xwallpaper) |
| dunst       | A highly configurable and lightweight notification daemon.                                 | [dunstrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/dunst/dunstrc)                  | [README](https://github.com/dunst-project/dunst/blob/master/README.md)   | N/A                                                                     | [dunst](https://github.com/dunst-project/dunst)        |
| dunstctl    | Command line control utility for dunst.                                                    | N/A                                                                                                               | [Man Page](https://man.archlinux.org/man/dunstctl.1.en)                  | N/A                                                                     | [dunstctl](https://github.com/dunst-project/dunst)     |
| polybar     | A fast and easy-to-use tool for creating status bars.                                      | [config](OOOO)                                                                                                    | [README](https://github.com/polybar/polybar/blob/master/README.md)       | N/A                                                                     | [polybar](https://github.com/polybar/polybar)          |
| slop        | An application that queries for a selection from the user and prints the region to stdout. | N/A                                                                                                               | [README](https://github.com/naelstrof/slop/blob/master/README.md)        | N/A                                                                     | [slop](https://github.com/naelstrof/slop)              |
| xautolock   | Monitors console activity under the X window system and runs a specified program.          | N/A                                                                                                               | [README](https://github.com/l0b0/xautolock/blob/master/Readme)           | N/A                                                                     | [xautolock](https://github.com/l0b0/xautolock)         |
| xmenu       | A menu utility for X.                                                                      | [menu-entires](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/xmenu-select/menu-entries) | [README]()                                                               | [FOSSilized_Daemon's xmenu](https://gitlab.com/FOSSilized_Daemon/xmenu) | [xmenu](https://github.com/phillbush/xmenu)            |
| sxhkd       | A X daemon that reacts to input events by executing commands.                              | [sxhkdrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/sxhkd/sxhkdrc)                  | [README](https://github.com/baskerville/sxhkd/blob/master/README.md)     | N/A                                                                     | [sxhkd](https://github.com/baskerville/sxhkd)          |
| picom       | A standalone compositor for Xorg.                                                          | [picom.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/master/src/configs/picom/picom.conf)            | [README](https://github.com/yshui/picom/blob/master/README.md)           | N/A                                                                     | [picom](https://github.com/yshui/picom/)               |
| xset        |
| xmodmap     |
| xorg-server |

## Media Applications
| Name       | Description                                                       | Configuration File                                                                                                                                                                                    | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| mpd        | A flexible, powerful, server-side application for playing music.  | [mpd.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/mpd/mpd.conf)                                                                                                      | [Site](https://www.musicpd.org/doc/html/user.html)                       | N/A                                                                 | [mpd](https://www.musicpd.org/)                        |
| mpc        | A minimalist command line interface to MPD.                       | N/A                                                                                                                                                                                                   | [Man Page](https://linux.die.net/man/1/mpc)                              | N/A                                                                 | [mpc](https://www.musicpd.org/clients/mpc/)            |
| mpv        | A free (as in freedom) media player for the command line.         | [mpv.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/mpv/mpv.conf)/[input.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/mpv/input.conf) | [Site](https://mpv.io/manual/)                                           | N/A                                                                 | [mpv](https://mpv.io/)                                 |

## Editing Applications
| Name       | Description                                                       | Configuration File                                                                                | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :-------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| neovim     | A hyperextensible Vim-based text editor.                          | [init.vim](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/nvim/init.vim) | [Site](https://neovim.io/doc/)                                           | N/A                                                                 | [Neovim](https://neovim.io/)                           |
| Blender    |
| Audacity   |
| Kid3       |
| GIMP       |

## Gaming Applications
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| Steam      |
| Itch.io    |
| Lutris     |

## Emulation Applications
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| DOSBox     |
| Scummvm    |
| Wine       |

## Communication Applications
| Name       | Description                                                       | Configuration File                                                                               | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----------| :-----------------------------------------------------------------| :------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| Irssi      |
| Profanity  |
| Discord    |
| Neomutt    |
