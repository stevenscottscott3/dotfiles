#!/bin/sh

###############################
# Handle Creating Directories #
###############################
create_parent_directories() {
	for target_directory in "${HOME}"/dl      "${HOME}"/dox    "${HOME}"/music   "${HOME}"/pix    "${HOME}"/vids\
                            "${HOME}"/.builds "${HOME}"/.cache "${HOME}"/.config "${HOME}"/.local
	do
		if ! [ -d "${target_directory}" ]; then
			mkdir -p "${target_directory}"
		fi
	done
}

create_sub_directories() {
	for target_directory in "${HOME}"/dl/firefox/ "${HOME}"/dl/transmission-cli "${HOME}"/dl/misc "${HOME}"/dox/dev\
		"${HOME}"/dox/dev/core-software/finished-projects "${HOME}"/dox/dev/core-software/working-projects\
		"${HOME}"/dox/dev/misc "${HOME}"/dox/dev/misc/X11-Utilities\
		"${HOME}"/dox/notes "${HOME}"/dox/work/school/gsu/english/english-composition-1102\
		"${HOME}"/dox/work/school/gsu/sociology/introduction-to-sociology-1101 "${HOME}"/dox/work/school/gsu/film/history-of-motion-pictures-1101\
		"${HOME}"/dox/work/school/gsu/misc/ "${HOME}"/dox/work/school/gsu/history/american-government-1102\
		"${HOME}"/dox/work/video-editing/youtube/404GenderNotFound "${HOME}"/dox/work/video-editing/youtube/UQ "${HOME}"/pix/screenshots\
		"${HOME}"/.local/bin "${HOME}"/.local/fonts "${HOME}"/.local/share "${HOME}"/.local/share/lists "${HOME}"/.cache/zsh "${HOME}"/.cache/nvim/undo "${HOME}"/.config/xmenu-select
	do
		if ! [ -d "${target_directory}" ]; then
			mkdir -p "${target_directory}"
		fi
	done
}

######################
# Clone Repositories #
######################
clone_personal_repositories() {
	# X11 utilities.
	git clone https://gitlab.com/FOSSilized_Daemon/dwm.git             "${HOME}"/dox/dev/misc/X11-Utilities/dwm
	git clone https://gitlab.com/FOSSilized_Daemon/xmenu.git           "${HOME}"/dox/dev/misc/X11-Utilities/xmenu

	# Finished projects.
	git clone https://gitlab.com/FOSSilized_Daemon/cnotes.git          "${HOME}"/dox/dev/core-software/working-projects/cnotes
	git clone https://gitlab.com/FOSSilized_Daemon/sdmu.git            "${HOME}"/dox/dev/core-software/working-projects/sdmu

	# Working projects.
	git clone https://gitlab.com/FOSSilized_Daemon/gmm.git             "${HOME}"/dox/dev/core-software/working-projects/gmm
	git clone https://gitlab.com/FOSSilized_Daemon/auhpt.git           "${HOME}"/dox/dev/core-software/working-projects/auhpt
	git clone https://gitlab.com/FOSSilized_Daemon/gal.git             "${HOME}"/dox/dev/core-software/working-projects/gal
	git clone https://gitlab.com/FOSSilized_Daemon/snmu.git            "${HOME}"/dox/dev/core-software/working-projects/snmu
	git clone https://gitlab.com/FOSSilized_Daemon/gsm.git             "${HOME}"/dox/dev/core-software/working-projects/gsm
	git clone https://gitlab.com/FOSSilized_Daemon/sup.git             "${HOME}"/dox/dev/core-software/working-projects/sup
	git clone https://gitlab.com/FOSSilized_Daemon/todo-manager.git    "${HOME}"/dox/dev/core-software/working-projects/todo-manager
	git clone https://gitlab.com/FOSSilized_Daemon/smusb.git           "${HOME}"/dox/dev/core-software/working-projects/smusb
	git clone https://gitlab.com/FOSSilized_Daemon/gdmu.git            "${HOME}"/dox/dev/core-software/working-projects/gdmu
	git clone https://gitlab.com/FOSSilized_Daemon/swqu.git            "${HOME}"/dox/dev/core-software/working-projects/swqu
	git clone https://gitlab.com/FOSSilized_Daemon/swbu.git            "${HOME}"/dox/dev/core-software/working-projects/swbu
	git clone https://gitlab.com/FOSSilized_Daemon/gffm.git            "${HOME}"/dox/dev/core-software/working-projects/gffm
	git clone https://gitlab.com/FOSSilized_Daemon/shqu.git            "${HOME}"/dox/dev/core-software/working-projects/shqu

	# Misc Projects.
	git clone https://gitlab.com/FOSSilized_Daemon/dotfiles.git        "${HOME}"/dox/dev/misc/dotfiles
	git clone https://gitlab.com/FOSSilized_Daemon/nexus.git           "${HOME}"/dox/dev/misc/nexus
	git clone https://gitlab.com/FOSSilized_Daemon/overview.git        "${HOME}"/dox/dev/misc/overview
	git clone https://gitlab.com/FOSSilized_Daemon/test-repository.git "${HOME}"/dox/dev/misc/test-repository
}

##############################
# Handle Installing Dotfiles #
##############################
install_configuration_files() {
	ln -s ~/dox/dev/misc/dotfiles/src/configs/dunst                ~/.config/dunst
	ln -s ~/dox/dev/misc/dotfiles/src/configs/mpd                  ~/.config/mpd
	ln -s ~/dox/dev/misc/dotfiles/src/configs/nvim                 ~/.config/nvim
	ln -s ~/dox/dev/misc/dotfiles/src/configs/zsh                  ~/.config/zsh
	ln -s ~/dox/dev/misc/dotfiles/src/configs/sxhkd                ~/.config/sxhkd
	ln -s ~/dox/dev/misc/dotfiles/src/configs/xmenu-select         ~/.configs/xmenu-select
	ln -s ~/dox/dev/misc/dotfiles/src/configs/.xinitrc             ~/.xinitrc
	ln -s ~/dox/dev/misc/dotfiles/src/configs/.zshenv              ~/.zshenv
	ln -s ~/dox/dev/misc/dotfiles/src/configs/.tmux.conf           ~/.tmux.conf
}

install_extras() {
	ln -s ~/dox/dev/misc/dotfiles/src/extras/wallpapers     ~/pix/wallpapers
	cp -r ~/dox/dev/misc/dotfiles/src/extras/.local/bin/*   ~/.local/bin
	cp -r ~/dox/dev/misc/dotfiles/src/extras/.local/fonts/* ~/.local/fonts
	cp -r ~/dox/dev/misc/dotfiles/src/extras/.local/share/* ~/.local/share
}

######################
# Main Functionality #
######################
init_dotfiles() {
	create_parent_directories
	create_sub_directories
	clone_personal_repositories
	install_configuration_files
	install_extras
}

init_dotfiles
