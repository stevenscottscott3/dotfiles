""""""""""""""""""""""""""
" Behavior Modifications "
""""""""""""""""""""""""""
set autoindent
set autoread
set backspace=indent,eol,start
set history=200
set ignorecase
set smartcase
set noshowmode
set showcmd
set incsearch
set ttyfast
set lazyredraw
set path+=**
set timeoutlen=1000
set ttimeoutlen=0
set formatoptions-=cro
set mouse=a

""""""""""""""""""""""""
"" Scrolling Settings ""
""""""""""""""""""""""""
set scrolloff=3
set sidescrolloff=5
set sidescroll=1

"""""""""""""""""""""
"" Denote Keywords ""
"""""""""""""""""""""
set iskeyword+=-

"""""""""""""""""""""""""""""
"" Copy and Paste Settings ""
"""""""""""""""""""""""""""""
set clipboard+=unnamedplus

"""""""""""""""""""""""
"" WildMenu Settings ""
"""""""""""""""""""""""
set wildmenu
set wildcharm=<C-Z>

""""""""""""""""""""
"" Netrw Settings ""
""""""""""""""""""""
let g:netrw_dirhistmax=20
let g:netrw_dirhistcnt=1

"""""""""""""""""""
"" Fold Settings ""
"""""""""""""""""""
augroup init.vim
	au BufReadPre * setlocal foldmethod=indent
	au BufWinEnter * if &fdm == 'indent' | setlocal foldmethod=manual | endif
augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Show Characters (Such as Tabs, Whitespace, Etc) ""
"""""""""""""""""""""""""""""""""""""""""""""""""""""
set list
set listchars=nbsp:¬,tab:»·,trail:·,extends:>

""""""""""""""""""
"" Tab Settings ""
""""""""""""""""""
set shiftwidth=4
set softtabstop=4
set tabstop=4

""""""""""""""""
"" Swap Files ""
""""""""""""""""
" Store swap files in either $HOME/.cache/nvim/swap the current working directory.
if isdirectory($HOME . '/.cache/nvim/swap') == 0
	:silent !mkdir -p ~/.cache/nvim/swap
endif

set directory=~/.cache/nvim/swap//
set directory+=.

"""""""""""""""""
"" Shada Files ""
"""""""""""""""""
" Store shada files in $HOME/.cache/nvim/shadas.
if isdirectory($HOME . '/.cache/nvim/shadas') == 0
	:silent !mkdir -p ~/.cache/nvim/shadas
endif

""""""""""""""""
"" Save Undos ""
""""""""""""""""
" Store undo files in $HOME/.cache/nvim/undos.
if isdirectory($HOME . '/.cache/nvim/undos') == 0
	:silent !mkdir -p ~/.cache/nvim/undos
endif

set undodir=~/.cache/nvim/undos
set undofile

"""""""""""""""""""""
"" Backup Settings ""
"""""""""""""""""""""
" Store backup files in $HOME/.local/share/nvim/backups or the current working directory using the '.bac' extension.
if isdirectory($HOME . '/.local/share/nvim/backups') == 0
	:silent !mkdir -p ~/.local/share/nvim/backups
endif

set backupdir-=.
set backupdir+=~/.local/share/nvim/backups
set backupdir+=.
set backupext=.bac
set writebackup
set backup

"""""""""""""""""""""
"" Auto-Completion ""
"""""""""""""""""""""
set complete+=kspell
set completeopt=menuone,longest
set shortmess+=c

""""""""""""""""
" Key Bindings "
""""""""""""""""
"""""""""""""""
"" Functions ""
"""""""""""""""
fun s:bak(f) abort
	execute('tabe' . a:f)
endfun

fun s:Bakfiles(...) abort
	let paths=filter(split(&path,','), {_,p -> len(p)})
	let glob=join(map(paths,{_,p -> glob(p .'/*.bac')}))
	return glob
endfun

" Use , as leader key.
let mapleader=","

" Source init.vim using F5.
map <silent> <F5> :source ~/.config/nvim/init.vim<CR>

" Toggle spell checking using F2.
map <silent> <F2> :set spell spelllang=en_us<CR>

" Toggle line wrapping using CTRL + l.
nnoremap <silent> <leader>l :set wrap!<CR>

" Edit a new file in the same directory with leader+e.
nnoremap <leader>e :vsp <C-R>=expand('%:p:h') . '/'<CR>

" Edit a backup file with leader+E. (FIX ME)
nnoremap <leader>E command! -complete=custom,s:Bakfiles -nargs=1 BAK call s:bak('<args>')<CR>

" Open the current file in $BROWSER with leader+o.
nnoremap <silent> <leader>o :!"${BROWSER}" %<CR>:redraw<CR>

" Move visually selected lines up or down in visual mode.
vnoremap K :m .-2<CR>==
vnoremap J :m .+1<CR>==
vnoremap K :m '<-2<CR>gv=gv
vnoremap J :m '>+1<CR>gv=gv

" Save file with Zz.
nnoremap <silent>Zz :update<CR>

" Create splits using [ and ].
nnoremap <silent> <leader>[ :vsp<CR>
nnoremap <silent> <leader>] :sp<CR>

" Close split using p.
nnoremap <silent> <leader>p :close<CR>

" Navigate splits using CTRL+hjkl
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Navigate wildmenu with CTRL+hjkl.
cnoremap <expr> <C-K> wildmenumode() ? "<C-P>" : "<C-K>"
cnoremap <expr> <C-J> wildmenumode() ? "<C-N>" : "<C-J>"
cnoremap <expr> <C-L> wildmenumode() ? "<C-Y>" : "<C-L>"
cnoremap <expr> <C-H> wildmenumode() ? "<C-E>" : "<C-E>"
"cnoremap <expr> <Tab> wildmenumode() ? "<C-Y>" : "<Tab>" (FIX ME)

" Navigate auto-completion with CTRL+hjkl.
inoremap <expr> <C-H> pumvisible() ? "<C-E>" : "<C-H>"
inoremap <expr> <C-L> pumvisible() ? "<C-Y>" : "<C-L>"
inoremap <expr> <C-J> pumvisible() ? "<C-N>" : "<C-J>"
inoremap <expr> <C-K> pumvisible() ? "<C-P>" : "<C-K>"
inoremap <expr> <Tab> pumvisible() ? "<C-Y>" : "<Tab>"

"""""""""""""""""
" Auto Commands "
"""""""""""""""""
autocmd BufWritePre * %s/\s\+$//e
autocmd FileType * setlocal formatoptions-=cro
autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
autocmd FileType * set spell spelllang=en_us

""""""""""
" Plugins "
"""""""""""
set packpath+=~/.config/nvim
packloadall

"""""""""""""""
"" Syntastic ""
"""""""""""""""
let g:syntastic_always_populate_loc_list=1
let g:syntastic_loc_list_height=5
let g:syntastic_auto_loc_list=0
let g:syntastic_shellcheck_sh_args="-s sh"
let g:syntastic_check_on_open=1
let g:syntastic_check_on_wq=1

""""""""""""""""""""
" UI Customization "
""""""""""""""""""""
syntax on
set number
set relativenumber
set encoding=utf-8
filetype plugin indent on
set cursorline
set cursorcolumn
set colorcolumn=120

"""""""""""""""""""""
"" Status Settings ""
"""""""""""""""""""""
let g:currentmode = {
	\ 'n'  : 'N ',
	\ 'no' : 'N·Operator Pending ',
	\ 'v'  : 'V ',
	\ 'V'  : 'V·Line ',
	\ "\<C-V>" : 'V·Block ',
	\ 's'  : 'Select ',
	\ 'S'  : 'S·Line ',
	\ "\<C-S>" : 'S·Block ',
	\ 'i'  : 'I ',
	\ 'R'  : 'R ',
	\ 'Rv' : 'V·Replace ',
	\ 'c'  : 'Command ',
	\ 'cv' : 'Vim Ex ',
	\ 'ce' : 'Ex ',
	\ 'r'  : 'Prompt ',
	\ 'rm' : 'More ',
	\ 'r?' : 'Confirm ',
	\ '!'  : 'Shell ',
	\ 't'  : 'Terminal '
\}

function! ChangeStatuslineColor()
	if (mode() =~# '\v(n|no)')
		exe 'hi! StatusLine ctermfg=008'
	elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
		exe 'hi! StatusLine ctermfg=005'
	elseif (mode() ==# 'i')
		exe 'hi! StatusLine ctermfg=004'
	else
		exe 'hi! StatusLine ctermfg=006'
	endif
	return ''
endfunction

function! FileSize()
	let bytes = getfsize(expand('%:p'))

	if (bytes >= 1024)
		let kbytes = bytes / 1024
	endif

	if (exists('kbytes') && kbytes >= 1000)
		let mbytes = kbytes / 1000
	endif

	if bytes <= 0
		return '0'
	endif

	if (exists('mbytes'))
		return mbytes . 'MB '
	elseif (exists('kbytes'))
		return kbytes . 'KB '
	else
		return bytes . 'B '
	endif
endfunction

function! ReadOnly()
	if &readonly || !&modifiable
		return ''
	else
		return ''
endfunction

function! GitBranch()
	return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
	let l:branchname = GitBranch()
	return strlen(l:branchname) > 0 ? ''.l:branchname.' ':''
endfunction

set laststatus=2
set statusline=
set statusline+=%{ChangeStatuslineColor()}
set statusline+=%0*\ %{toupper(g:currentmode[mode()])}
set statusline+=%8*\ [%n]
set statusline+=%8*\ %{StatuslineGit()}
set statusline+=%8*\ %<%F\ %{ReadOnly()}\ %m\ %w
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set statusline+=%9*\ %=
set statusline+=%8*\ %y
set statusline+=%7*\ %{(&fenc!=''?&fenc:&enc)}\[%{&ff}]
set statusline+=%8*\ %-3(%{FileSize()}%)
set statusline+=%0*\ %3p%%\ \ %l:\ %3c

""""""""""""""""""
"" Theme Colors ""
""""""""""""""""""
if version > 580
	hi clear

	if exists("syntax_on")
		syntax reset
	endif
endif

"""""""""""""""""""
""" Text Colors """
"""""""""""""""""""
" General
hi Normal           ctermfg=51      ctermbg=NONE    cterm=NONE
hi Underlined       ctermfg=45      ctermbg=NONE    cterm=UNDERLINE
hi Number           ctermfg=201     ctermbg=NONE    cterm=NONE
hi MatchParen       ctermfg=54      ctermbg=NONE    cterm=UNDERLINE

" Spelling
hi SpellBad         ctermfg=58      ctermbg=NONE    cterm=UNDERCURL
hi SpellCap         ctermfg=58      ctermbg=NONE    cterm=UNDERCURL
hi SpellLocal       ctermfg=58      ctermbg=NONE    cterm=UNDERCURL
hi SpellRare        ctermfg=58      ctermbg=NONE    cterm=UNDERCURL

" Searching
hi IncSearch        ctermfg=127     ctermbg=NONE    cterm=BOLD
hi Search           ctermfg=89      ctermbg=NONE    cterm=BOLD,UNDERLINE

" Coding
hi Comment          ctermfg=54      ctermbg=NONE    cterm=BOLD
hi SpecialComment   ctermfg=92      ctermbg=NONE    cterm=BOLD
hi Todo             ctermfg=70      ctermbg=NONE    cterm=UNDERLINE

"""""""""""""""""""""
""" Cursor Colors """
"""""""""""""""""""""
hi Cursor           ctermfg=199     ctermbg=NONE    cterm=BOLD
hi iCursor          ctermfg=123     ctermbg=NONE    cterm=BOLD

:set guicursor=n-v-c:block-Cursor,i-ci-ve:ver25-iCursor,r-cr:hor20,o:hor50
			\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
			\,sm:block-blinkwait175-blinkoff150-blinkon175
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=0

"""""""""""""""""""
""" Fold Colors """
"""""""""""""""""""
hi Folded           ctermfg=54     ctermbg=NONE    cterm=BOLD
hi FoldColumn       ctermfg=54     ctermbg=NONE    cterm=BOLD

"""""""""""""""""""""""""""""""""""""
""" Line Number and Column Colors """
"""""""""""""""""""""""""""""""""""""
hi LineNr           ctermfg=142     ctermbg=NONE    cterm=NONE
hi CursorLineNr     ctermfg=154     ctermbg=NONE    cterm=BOLD
hi ColorColumn      ctermfg=NONE    ctermbg=234     cterm=NONE

""""""""""""""""""""""""""
""" Cursor Line Colors """
""""""""""""""""""""""""""
hi CursorColumn     ctermfg=NONE    ctermbg=234     cterm=NONE
hi CursorLine       ctermfg=NONE    ctermbg=234     cterm=NONE

""""""""""""""""""""
""" Error Colors """
""""""""""""""""""""
hi Error            ctermfg=196     ctermbg=NONE    cterm=BOLD
hi ErrorMsg         ctermfg=196     ctermbg=NONE    cterm=BOLD
hi WarningMsg       ctermfg=196     ctermbg=NONE    cterm=BOLD
hi MoreMsg          ctermfg=196     ctermbg=NONE    cterm=BOLD

" Syntastic
hi link SyntasticErrorSign        ErrorMsg
hi link SyntasticWarningSign      ErrorMsg
hi link SyntasticStyleErrorSign   ErrorMsg
hi link SyntasticStyleWarningSign ErrorMsg

""""""""""""""""""""""""
""" Highlight Colors """
""""""""""""""""""""""""
hi VISUAL           ctermfg=199     ctermbg=NONE    cterm=BOLD
hi VisualNOS        ctermfg=NONE    ctermbg=NONE    cterm=UNDERLINE

""""""""""""""""""""""""""
""" Status Line Colors """
""""""""""""""""""""""""""
hi StatusLine       ctermfg=93      ctermbg=NONE    cterm=BOLD
hi StatusLineNC     ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User1            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User2            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User3            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User4            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User5            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User7            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User8            ctermfg=93      ctermbg=NONE    cterm=BOLD
hi User9            ctermfg=93      ctermbg=NONE    cterm=BOLD







hi Title            ctermfg=15      ctermbg=NONE    cterm=UNDERLINE
hi Directory        ctermfg=39      ctermbg=NONE    cterm=NONE
hi SignColumn       ctermfg=NONE    ctermbg=8       cterm=NONE
hi VertSplit        ctermfg=15      ctermbg=NONE    cterm=NONE
hi WildMenu         ctermfg=33      ctermbg=NONE    cterm=BOLD
hi ModeMsg          ctermfg=4       ctermfg=NONE    cterm=NONE
hi DiffAdd          ctermfg=NONE    ctermbg=237     cterm=NONE
hi DiffDelete       ctermfg=197     ctermbg=NONE    cterm=NONE
hi DiffChange       ctermfg=230     ctermbg=24      cterm=UNDERLINE
hi DiffText         ctermfg=230     ctermbg=53      cterm=NONE
hi Pmenu            ctermfg=15      ctermbg=NONE    cterm=NONE
hi PmenuSel         ctermfg=15      ctermbg=NONE    cterm=REVERSE
hi PmenuSbar        ctermfg=NONE    ctermbg=60      cterm=NONE
hi PmenuThumb       ctermfg=NONE    ctermbg=103     cterm=NONE
hi Question         ctermfg=4       ctermbg=NONE    cterm=NONE
hi TabLine          ctermfg=14      ctermbg=NONE    cterm=NONE
hi TabLineSel       ctermfg=15      ctermbg=33      cterm=REVERSE
hi TabLineFill      ctermfg=234     ctermbg=66      cterm=NONE
hi Ignore           ctermfg=NONE    ctermbg=NONE    cterm=NONE
hi Typedef          ctermfg=80      ctermbg=NONE    cterm=BOLD
hi Include          ctermfg=13      ctermbg=NONE    cterm=NONE
hi Float            ctermfg=197     ctermbg=NONE    cterm=NONE
hi NonText          ctermfg=46      ctermbg=NONE    cterm=NONE
hi Identifier       ctermfg=80      ctermbg=NONE    cterm=NONE
hi Conditional      ctermfg=214     ctermbg=NONE    cterm=BOLD
hi StorageClass     ctermfg=80      ctermbg=NONE    cterm=BOLD
hi Special          ctermfg=13      ctermbg=NONE    cterm=BOLD
hi Label            ctermfg=214     ctermbg=NONE    cterm=BOLD
hi Delimiter        ctermfg=101     ctermbg=NONE    cterm=NONE
hi Statement        ctermfg=214     ctermbg=NONE    cterm=BOLD
hi Character        ctermfg=197     ctermbg=NONE    cterm=NONE
hi Boolean          ctermfg=197     ctermbg=NONE    cterm=NONE
hi Define           ctermfg=101     ctermbg=NONE    cterm=NONE
hi Function         ctermfg=80      ctermbg=NONE    cterm=BOLD
hi PreProc          ctermfg=13      ctermbg=NONE    cterm=NONE
hi Exception        ctermfg=214     ctermbg=NONE    cterm=BOLD
hi Keyword          ctermfg=220     ctermbg=NONE    cterm=BOLD
hi Type             ctermfg=80      ctermbg=NONE    cterm=BOLD
hi SpecialKey       ctermfg=220     ctermbg=NONE    cterm=NONE
hi Constant         ctermfg=197     ctermbg=NONE    cterm=NONE
hi Tag              ctermfg=214     ctermbg=NONE    cterm=NONE
hi String           ctermfg=76      ctermbg=NONE    cterm=NONE
hi Repeat           ctermfg=214     ctermbg=NONE    cterm=BOLD
hi Structure        ctermfg=214     ctermbg=NONE    cterm=BOLD
hi Macro            ctermfg=220     ctermbg=NONE    cterm=NONE
hi cursorim         ctermfg=234     ctermbg=60      cterm=NONE
