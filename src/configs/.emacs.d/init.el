;; TODO:
; - auto-complete (like in vim)
; - enter causes massive tabbing sometimes
; - evil mode copy to xclip
; - open current file in $BROWSER
; - modal cursor based on mode (like in nvim)
; - move visually selected lines using j/k in visual mode (like in vim)
; - don't show visual EOB
; - GOYO like mode
; - vim keys in buffer select etc
; - write theme

;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Global Declartions ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions
(defun insert-a-tab-character ()
  "Let the TAB key insert a tab character."
  (interactive)
  (insert "\t"))

(defun toggle-immersion-mode ()
  "Turn off settings that break immersion (such as line numbers), and then enable Olivetti."
  (display-line-numbers-mode 0)
  (olivetti-mode))

(defun dired-list-backups ()
  "Prompt user to select a file from the backups directory, then open it in a new buffer."
  (interactive)
  (setq backup-directory-alist `(("." . "~/.local/share/emacs/backups")))
  (dired (file-name-directory) (make-backup-file-name) (or (buffer-file-name) "/") "-v"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Behavoir Moditications ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Don't automatically save changes.
(setq auto-save-default nil)

;; Use UTF-8 encoding.
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; Don't add two spaces after full-stop.
(setq end-double-space nil)

;; Set proper indentation.
(setq default-tab-width 4)
(setq tab-width 4)
(setq default-fil-column 80)
(setq-default evil-indent-convert-tabs nil)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default evil-shift-round nil)

;; Enable mouse support.
(xterm-mouse-mode 1)

;; Scroll 'n' lines to screen edge.
(setq scroll-margin 2)

;; Scroll back 'n' number of lines to bring the cursor back on screen.
(setq scroll-conservatively scroll-margin)

;; Keyboard scroll one line at a time.
(setq scroll-step 1)

;; Mouse scroll 'n' lines.
(setq mouse-wheel-scroll-amount '(6 ((shift) . 1)))

;; Don't accelerate scrolling.
(setq mouse-wheel-progressive-speed nil)

;; Don't use a timer when scrolling.
(setq mouse-wheel-inhibit-click-time nil)

;; Preserve line/column.
(setq scroll-preserve-screen-position t)

;; Move the cursor to the top/bottom even if the screen it viewing the top/bottom.
(setq scroll-error-top-bottom t)

;; Center after going to the next compiler error.
(setq next-error-recenter (quote (4)))

;; Always draw immediately when scrolling.
(setq fast-but-imprecise-scrolling nil)
(setq jit-lock-defer-time 0)

;; Don't group undo steps.
(fset 'undo-auto-amalgamate 'ignore)

;; Increase undo limits.
(setq undo-limit 6710886400)

;; Set a strong undo limit of 96mb.
(setq undo-strong-limit 100663296)

;; Set undo outer limit to 960mb.
(setq undo-outer-limit 1006632960)

;; Use case-insensitive searching.
(setq-default case-fold-search t)
(setq dabbrev-case-fold-search t)
(setq-default search-upper-case t)

;; Store backup files in ~/.local/emacs/backups.
(setq
 backup-by-copying t
 backup-directory-alist
 '(("." . "~/.local/emacs/backups/"))
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

;; Save the cursors position in the file.
(save-place-mode 1)


;; Remove all trailing whitespace when saving a file.
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;;;;;;;;;;;;;;;
;;; Packages ;;;
;;;;;;;;;;;;;;;;
;; Set package archives.
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("gnu" . "https//elpa.gnu.org/packages/")))

(package-initialize)

;; Automatically install packages.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))
(setq use-package-always-ensure t)

;; Don't automatically load packages, lean on modes instead.
(setq use-package-always-defer t)

;; Load EVIL mode.
(use-package evil
  :demand t
  :init

  ;; Enable EVIL mode.
  (evil-mode)

  ;; Use EVIL mode search by default.
  (setq evil-search-module 'evil-search)
  (setq evil-ex-search-case 'smart))

;; Load SLIME.
(use-package slime)

;; Load Olivetti
(use-package olivetti)

;;;;;;;;;;;;;;;;;;;;
;;; Key Bindings ;;;
;;;;;;;;;;;;;;;;;;;;
;; Escape quits prompts.
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; TAB inserts a '\t' character.
(global-set-key (kbd "TAB") 'insert-a-tab-character)
(global-set-key (kbd "<tab>") 'insert-a-tab-character)

;; Toggle spell checking using F2.
(global-set-key (kbd "<f2>") 'flyspell-mode)

;; Toggle immersion mode using F3.
(global-set-key (kbd "<f3>") 'toggle-immersion-mode)

;; Source init.el using F5.
;(global-set-key (kbd "<f5>") 'load-file ~/.config/emacs/init.el)

;; Save the current file using Zz.
(evil-define-key 'normal 'global (kbd "Zz") 'save-buffer)

;; Copy and paste to system.

;; Set leader key to ,.
(with-eval-after-load 'evil
  (evil-set-leader '(normal) (kbd ",")))

;; Close current split using leader + p.
(evil-define-key 'normal 'global (kbd "<leader>p") 'kill-current-buffer)

;; Toggle line wrapping using leader + l.
(evil-define-key 'normal 'global (kbd "<leader>l") 'toggle-truncate-lines)

;; Open a file within the current directory using leader + e.
(evil-define-key 'normal 'global (kbd "<leader>e") 'find-file)

;; Launch SLIME using leader + s.
(evil-define-key 'normal 'global (kbd "<leader>s") 'slime)

;; Go forward and backward through the buffers list.
(evil-define-key 'normal 'global (kbd "<leader>n") 'next-buffer)
(evil-define-key 'normal 'global (kbd "<leader>p") 'previous-buffer)

;; Open a backup file using leader + E.
(evil-define-key 'normal 'global (kbd "<leader>E") 'dired-list-backups)

;; Open the current file in $BROWSER using leader + o.
;(evil-define-key 'normal 'global (kbd "<leader>o") '

;; Move visually selected lines up and down in visual mode using j and k.
;(evil-define-key 'visual 'global (kbd "<leader>j") '
;(evil-define-key 'visual 'global (kbd "<leader>k") '

;; Navigate auto-completion menu using control + h, j, k, and l.
;(evil-define-key 'normal 'global (kbd "<leader>h") '
;(evil-define-key 'normal 'global (kbd "<leader>j") '
;(evil-define-key 'normal 'global (kbd "<leader>k") '
;(evil-define-key 'normal 'global (kbd "<leader>l") '

;; Select auto-completion menu using Tab and Enter.
;(evil-define-key 'normal 'global (kbd "<tab") '
;(evil-define-key 'normal 'global (kbd "enter") '

;;;;;;;;;;;;;;;;;;;;;;;;
;;; UI Customization ;;;
;;;;;;;;;;;;;;;;;;;;;;;;
;; Disable GUI elements.
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(set-fringe-mode 10)

;; Disable the visual bell.
(setq visible-bell nil)

;; Disable startup message.
(defun display-startup-echo-areas-message ()
  (message ""))

(setq inhibit-startup-screen t)

;; Don't show the buffer list on startup.
(setq inhibit-startup-buffer-menu t)

;; Hide the mouse cursor while typing.
(setq make-pointer-invisible t)

;; Show empty lines.
(setq-default indicate-empty-lines t)

;; Only highlight cursors and syntax in current windows.
(setq current-in-non-selected-windows 'hollow)
(setq highlight-nonselected-windows t)
(setq bidi-display-reordering nil)

;; Show line numbers.
(setq-default display-line-numbers-type 'visual)
(global-display-line-numbers-mode 1)

;; Show matching parentheses.
(show-paren-mode 1)

;; Display whitespace.
(require 'whitespace)
(global-whitespace-mode)
(setq-default show-trailing-whitespace t)

;; Disable visual end of buffer (FIX ME)
(setq ring-bell-function nil
  visible-bell nil)

;; Highlight spaces, tabs, and newline.
(setq whitespace-style
 (quote (space tabs space-mark tab-mark )))

(setq whitespace-display-mappings
 '(
   (space-mark 32 [183] [46])
   (tab-mark 9 [9654 9] [92 9])))

;; Set the cursor based on the current mode.
(setq evil-normal-state-cursor '((box))
      evil-insert-state-cursor '((hbar . 5))
      evil-visual-state-cursor '((bar))
      evil-replace-state-cursor '((hollow)))

;; Highlight the current line and column.
;(hl-line-mode)

;; Load theme.
(load-theme 'acid)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("627087facbd254fb9d48e1f05c589f776ee19177fb09b84c639739239223f3dd" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
