;;; acid-theme.el --- acid
;;; Version: 1.0
;;; Commentary:
;;; A theme called acid
;;; Code:

(deftheme acid "DOCSTRING for acid")
  (custom-theme-set-faces 'acid
   '(default ((t (:foreground "#00d2ff" :background "#000000" ))))
   '(cursor ((t (:background "#ff00f8" ))))
   '(fringe ((t (:background "#282828" ))))
   '(mode-line ((t (:foreground "#282828" :background "#7c6f64" ))))
   '(region ((t (:background "#2d2d2d" ))))
   '(secondary-selection ((t (:background "#252422" ))))
   '(font-lock-builtin-face ((t (:foreground "#ff7300" ))))
   '(font-lock-comment-face ((t (:foreground "#6700a3" ))))
   '(font-lock-function-name-face ((t (:foreground "#ffd600" ))))
   '(font-lock-keyword-face ((t (:foreground "#ff00ec" ))))
   '(font-lock-string-face ((t (:foreground "#00ff1e" ))))
   '(font-lock-type-face ((t (:foreground "#f0ff00" ))))
   '(font-lock-constant-face ((t (:foreground "#adff00" ))))
   '(font-lock-variable-name-face ((t (:foreground "#00ffff" ))))
   '(minibuffer-prompt ((t (:foreground "#b8bb26" :bold t ))))
   '(font-lock-warning-face ((t (:foreground "red" :bold t ))))
   )

;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))
;; Automatically add this theme to the load path

(provide-theme 'acid)

;;; acid-theme.el ends here
