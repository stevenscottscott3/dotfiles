###########################
# Application Launcher(s) #
###########################
super + {d, shift + d}
	{gal, dwm-signal riospawnx xmenu-select}

###################
# Terminal Chains #
###################
super + t ; {t,m}
	{st,st -- tmux}

######################
# Terminal Shortcuts #
######################
super + Return
	st

######################
# Web Browser Chains #
######################
super + b ; {m,s}
	{firefox,tor-browser}

#################
# Gaming Chains #
#################
super + g ; {s,i,l}
	{steam,itchio,lutris}

###############
# Help Chains #
###############
# super + h ; {h,o,w}

################
# Media Chains #
################
super + m ; {shift slash,shift comma,shift period}
	{mpc -q next, mpc -q prev, mpc -q toggle}

super + m ; y
	firefox -new-tab "https://www.youtube.com"

###################
# Media Shortcuts #
###################
super + {shift slash,shift comma,shift period}
	{mpc -q next, mpc -q prev, mpc -q toggle}

########################
# Communication Chains #
########################
super + c ; {i,x,d,e,shift e}
	{st -c 'irssi' -- irssi,st -c 'profanity' -- profanity,discord,st -c 'neomutt' -- neomutt,firefox -new-tab "https://www.protonmail.com"}

##################
# Editing Chains #
##################
super + e ; {e,v,a,m,i}
	{st -c 'nvim' -- nvim,blender,audacity,kid3,gimp}

#################
# System Chains #
#################
super + s ; {j,k}
	{pactl set-sink-volume 0 -5%, pactl set-sink-volume = +5%}

super + s ; {shift j,shift k}
	{xbacklight -inc 5, xbacklight -dec 5}

super + s ; shift + q
	gsm

#super + s ; r
#	current_sxhkd_pid=$(ps -ef | awk '/[s]xhkd/\{print $2\}') && kill -1 "$\{current_sxhkd_pid\}";\
#	notify-send -u low -t 5000 "Update: Restarting sxhkd."; sxhkd &; exit 0

#super + s ; {n, shift n, control n}
#	{dunstctl close,dunstctl closeall,dunstctl history-pop}

####################
# System Shortcuts #
####################
super + shift + q
	gsm

############################
# Window Management Chains #
############################
super + w ; {j,k}
	dwm-signal focusstack {+,-}1

super + w ; shift + {j,k}
	dwm-signal {pushdown,pushup}

super + w ; {_,ctrl +}{_,shift +} {1-9}
	dwm-signal {_,toggle}{viewex,tagex} {0-8}

super + w ; Tab
	dwm-signal view

super + w ; 0
	dwm-signal viewall

super + w ; {[,]}
	dwm-signal focusmon {-,+}1

super + w ; shift {[,]}
	dwm-signal tagmon {-,+}1

super + w ; {t,m,f}
	dwm-signal setlayoutex {0,1,2}

super + w ; {shift space, space}
	dwm-signal cyclelayout {-,+}1

super + w ; {h,l}
	dwm-signal setmfact {-,+}0.05

super + w ; shift + {h,v}
	dwm-signal incnmaster {+,-}1

super + w ; {minus,equal}
	dwm-signal setgaps {-,+}5

super + w ; shift {minus,equal}
	dwm-signal setborderpx {-,+}1

super + w ; button1
	dwm-signal rioresize

super + w ; shift + f
	dwm-signal togglefloating

super + w ; s
	dwm-signal togglesticky

super + w ; shift + s
	dwm-signal togglebar

super + w ; shift + p
	dwm-signal killclient

super + w ; shift + Return
	dwm-signal zoom

super + w ; control + {m,l}
	dwm-signal togglescratchpadx {0,1}

###############################
# Window Management Shortcuts #
###############################
super + {j,k}
	dwm-signal focusstack {+,-}1

super + shift + {j,k}
	dwm-signal {pushdown,pushup}

super + {_,ctrl +}{_,shift +} {1-9}
	dwm-signal {_,toggle}{viewex,tagex} {0-8}

super + Tab
	dwm-signal view

super + 0
	dwm-signal viewall

super + {[,]}
	dwm-signal focusmon {-,+}1

super + shift {[,]}
	dwm-signal tagmon {-,+}1

super + {shift space, space}
	dwm-signal cyclelayout {-,+}1

super + {h,l}
	dwm-signal setmfact {-,+}0.05

super + shift + {h,v}
	dwm-signal incnmaster {+,-}1

super + {minus,equal}
	dwm-signal setgaps {-,+}5

super + shift + {minus,equal}
	dwm-signal setborderpx {-,+}1

super + button1
	dwm-signal rioresize

super + s
	dwm-signal togglesticky

super + shift + p
	dwm-signal killclient

super + control + {m,l}
	dwm-signal togglescratchpadx {0,1}
