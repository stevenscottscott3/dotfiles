######################
# Handle Environment #
######################
start_environment() {
	printf "Start graphical user interface? (y/n/N): "
	read -r user_answer

	if [ "${user_answer}" = 'y' ] || [ "${user_answer}" = 'Y' ] || [ "${user_answer}" = 'yes' ] ||\
		[ "${user_answer}" = 'Yes' ]; then
		startx > /dev/null 2>&1
	elif command -v "${MULTIPLEXOR}" > /dev/null 2>&1; then
		"${MULTIPLEXOR}"
	fi
}

################################
# Handle Environment Variables #
################################
set_environment_variables() {
	# Directories containing installed programs (everything post "$HOME/.local/bin" is OS specific).
	export PATH=/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/bin/sbin:"${HOME}"/.local/bin:/snap/bin:/usr/games:"${HOME}"/.roswell/bin

	# Language settings.
	export LC_TYPE="en_US.UTF-8"
	export LANG=en_US.UTF-8

	# Program variables.
	export LS_COLORS='di=1;34:ln=35:so=32:pi=33:ex=31:bd=34;1;40:cd=34;40:su=31;40:sg=31;40:tw=1;33;40:ow=33;40'
	export VISUAL='emacs -nw'
	export EDITOR='emacs -nw'
	export PAGER=less
	export TERMINAL=st
	export WINDOW_MANAGER=dwm
	export MULTIPLEXOR=tmux
	export BROWSER=firefox
	export SECURE_BROWSER=tor-browser
	export GFFM_FUZZY_FINDER=fzf
	export GFFM_CLASS=-c
	export GFFM_GEOMETRY=-g
	export GFFM_WIDTH=40
	export GFFM_HEIGHT=10
	export GFFM_X_OFFSET=450
	export GFFM_Y_OFFSET=200

	# Directory and file variables.
	export DOTFILES_DIRECTORY="${HOME}/dox/dev/misc/dotfiles"
	export CNOTES_DIRECTORY="${HOME}/dox/notes/"

	# OOOO IMPLEMENT ME OOOO
	export UNICODE_LIST="${HOME}/.local/share/lists/unicode"
	export TORRENT_DIRECTORY="${HOME}/dl/transmission-cli"
	export TORRENT_BLOCKLIST="${HOME}/.local/share/lists/torrentblacklist"
	export FETCH_DIRECTORY="${HOME}/dl/misc"
	export GRAPHICAL_LOCKER=slock
	export PROMPT_MENU="dmenu -b -i -p"
}

###########################
# Handle General Settings #
###########################
set_general_settings() {
	umask 077
}

########
# Main #
########
init_profile() {
	set_environment_variables
	set_general_settings
	start_environment
}

init_profile
