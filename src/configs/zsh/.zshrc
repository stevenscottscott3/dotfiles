####################
# History Settings #
####################
set_history_settings() {
	HISTSIZE=10000
	SAVEHIST=10000
	HISTFILE="${HOME}"/.cache/zsh/history
}

###########################
# Tab Completion Settings #
###########################
#########################
## Tab Completion Mode ##
#########################
set_tab_completion_method() {
	# Basic tab completion (with support for hidden files).
	autoload -U compinit
	zstyle ':completion:*' menu select
	zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
	zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
	zmodload zsh/complist
	compinit
	_comp_options+=(globdots)
}

set_vi_tab_menu_navigation() {
	# Use vim keys in the tab completion menu.
	bindkey -M menuselect 'h' vi-backward-char
	bindkey -M menuselect 'k' vi-up-line-or-history
	bindkey -M menuselect 'l' vi-forward-char
	bindkey -M menuselect 'j' vi-down-line-or-history
}

########################
# Line Editor Settings #
########################
#######################
## Line Editing Mode ##
#######################
set_vi_mode() {
	# Set vi line editing mode with a one second timeout delay between modes.
	bindkey -v
	export KEYTIMEOUT=1
}

##################
## Modal Cursor ##
##################
zle-keymap-select() {
	if [[ ${KEYMAP} == vicmd ]] || [[ $1 = 'block' ]]; then
		printf '\e[1 q'
	elif [[ ${KEYMAP} == main ]] || [[ ${KEYMAP} == viins ]] || [[ ${KEYMAP} = '' ]] || [[ $1 = 'beam' ]]; then
		printf '\e[5 q'
	fi
}

zle-line-init() {
	# Start new shells in vi insert mode.
	printf '\e[5 q'
}

##################
## Key bindings ##
##################
###################
## Miscellaneous ##
###################
exec-zsh() {
	exec zsh < "${TTY}"
}

zle -N exec-zsh
autoload -Uz edit-command-line
zle -N edit-command-line

set_keybindings() {
	# Use CTRL+l,h to navigate.
	bindkey '^L' emacs-forward-word
	bindkey '^H' emacs-backward-word

	# Use BackSpace to delete a character.
	bindkey "^?" backward-delete-char

	# Reverse lookup with / and ? in normal mode and CTRL + b and CTRL + f in insert mode.
	bindkey -M vicmd '/'  history-incremental-pattern-search-backward
	bindkey -M vicmd '?'  history-incremental-pattern-search-forward
	bindkey -M viins '^b' history-incremental-pattern-search-backward
	bindkey -M viins '^f' history-incremental-pattern-search-forward

	# Restart zsh.
	bindkey '\e[15~' exec-zsh

	# Edit the current line in $VISUAL.
	bindkey '^E' edit-command-line
}

###########
# Aliases #
###########
#################
## Git Aliases ##
#################
git_add() {
	if [ -z "${1}" ]; then
		git add .
	else
		for untracked_file in "${@}"
		do
			git add "${untracked_file}"
		done
	fi
}

git_remove() {
	if [ -n "${1}" ]; then
		for tracked_file in "${@}"
		do
			if [ -d "${tracked_file}" ]; then
				git rm -rf "${tracked_file}"
			else
				git rm -f "${@}"
			fi
		done
	fi
}

git_reset() {
	current_branch=$(git branch | grep "^\*" | sed 's/*//' | sed 's/ //')

	if [ -e "${1}" ]; then
		for target_item in "${@}"
		do
			git checkout "${current_branch}" "${target_item}"
		done
	else
		git reset --hard
	fi
}

git_checkout() {
	if [ -z "${1}" ]; then
		git checkout master
	else
		git checkout "${1}"
	fi
}

git_branch() {
	if [ -z "${1}" ]; then
		git branch
	elif [ "${1}" = "-d" ] || [ "${1}" = "-D" ]; then
		git branch -d "${2}"
	elif [ "${1}" = "-m" ] || [ "${1}" = "-M" ]; then
		git branch -m "${2}"
	else
		git checkout -b "${1}"
	fi
}

git_pull() {
	if [ -z "${1}" ]; then
		git pull
	else
		git pull "${1}"
	fi
}

#######################
## Directory Aliases ##
#######################
#############################
### Downloads Directories ###
#############################
cd_dl() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dl ]; then
			mkdir -p "${HOME}"/dl
		fi
		cd "${HOME}"/dl || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dl/"${1}" ]; then
			mkdir -p "${HOME}"/dl/"${1}"
		fi
		cd "${HOME}"/dl/"${1}" || return 1
		ls -a --color
	fi
}

cd_dff() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dl/firefox ]; then
			mkdir -p "${HOME}"/dl/firefox
		fi
		cd "${HOME}"/dl/firefox || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dl/firefox/"${1}" ]; then
			mkdir -p "${HOME}"/dl/firefox/"${1}"
		fi
		cd "${HOME}"/dl/firefox/"${1}" || return 1
		ls -a --color
	fi
}

cd_dtc() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dl/transmission-cli ]; then
			mkdir -p "${HOME}"/dl/transmission-cli
		fi
		cd "${HOME}"/dl/transmission-cli || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dl/transmission-cli/"${1}" ]; then
			mkdir -p "${HOME}"/dl/transmission-cli/"${1}"
		fi
		cd "${HOME}"/dl/transmission-cli/"${1}" || return 1
		ls -a --color
	fi
}

cd_dlm() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dl/misc ]; then
			mkdir -p "${HOME}"/dl/misc
		fi
		cd "${HOME}"/dl/misc || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dl/misc/"${1}" ]; then
			mkdir -p "${HOME}"/dl/misc/"${1}"
		fi
		cd "${HOME}"/dl/misc/"${1}" || return 1
		ls -a --color
	fi
}

#############################
### Documents Directories ###
#############################
cd_dx() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox ]; then
			mkdir -p "${HOME}"/dox
		fi
		cd "${HOME}"/dox || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/"${1}" ]; then
			mkdir -p "${HOME}"/dox/"${1}"
		fi
		cd "${HOME}"/dox/"${1}" || return 1
		ls -a --color
	fi
}

cd_ddfp() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/core-software/finished-projects ]; then
			mkdir -p "${HOME}"/dox/dev/core-software/finished-projects
		fi
		cd "${HOME}"/dox/dev/core-software/finished-projects || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/core-software/finished-projects/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/core-software/finished-projects/"${1}"
		fi
		cd "${HOME}"/dox/dev/core-software/finished-projects/"${1}" || return 1
		ls -a --color
	fi
}

cd_ddwp() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/core-software/working-projects ]; then
			mkdir -p "${HOME}"/dox/dev/core-software/working-projects
		fi
		cd "${HOME}"/dox/dev/core-software/working-projects || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/core-software/working-projects/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/core-software/working-projects/"${1}"
		fi
		cd "${HOME}"/dox/dev/core-software/working-projects/"${1}" || return 1
		ls -a --color
	fi
}

cd_ddxm() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/misc ]; then
			mkdir -p "${HOME}"/dox/dev/misc
		fi
		cd "${HOME}"/dox/dev/misc || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/misc/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/misc/"${1}"
		fi
		cd "${HOME}"/dox/dev/misc/"${1}" || return 1
		ls -a --color
	fi
}

cd_dddf() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/misc/dotfiles ]; then
			mkdir -p "${HOME}"/dox/dev/misc/dotfiles
		fi
		cd "${HOME}"/dox/dev/misc/dotfiles || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/misc/dotfiles/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/misc/dotfiles/"${1}"
		fi
		cd "${HOME}"/dox/dev/misc/dotfiles/"${1}" || return 1
		ls -a --color
	fi
}

cd_ddx11() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/misc/X11-Utilities ]; then
			mkdir -p "${HOME}"/dox/dev/misc/X11-Utilities
		fi
		cd "${HOME}"/dox/dev/misc/X11-Utilities || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/misc/X11-Utilities/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/misc/X11-Utilities/"${1}"
		fi
		cd "${HOME}"/dox/dev/misc/X11-Utilities/"${1}" || return 1
		ls -a --color
	fi
}

cd_dn() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/notes ]; then
			mkdir -p "${HOME}"/dox/notes
		fi
		cd "${HOME}"/dox/notes || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/notes/"${1}" ]; then
			mkdir -p "${HOME}"/dox/notes/"${1}"
		fi
		cd "${HOME}"/dox/notes/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsg() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/school/gsu ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu
		fi
		cd "${HOME}"/dox/work/school/gsu || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/school/gsu/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/"${1}"
		fi
		cd "${HOME}"/dox/work/school/gsu/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsge() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/dev/work/school/gsu/english ]; then
			mkdir -p "${HOME}"/dox/dev/work/school/gsu/english
		fi
		cd "${HOME}"/dox/dev/work/school/gsu/english || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/dev/work/school/gsu/english/"${1}" ]; then
			mkdir -p "${HOME}"/dox/dev/work/school/gsu/english/"${1}"
		fi
		cd "${HOME}"/dox/dev/work/school/gsu/english/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsgs() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/school/gsu/sociology ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/sociology
		fi
		cd "${HOME}"/dox/work/school/gsu/sociology || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/school/gsu/sociology/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/sociology/"${1}"
		fi
		cd "${HOME}"/dox/work/school/gsu/sociology/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsgf() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/school/gsu/film ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/film
		fi
		cd "${HOME}"/dox/work/school/gsu/film || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/school/gsu/film/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/film/"${1}"
		fi
		cd "${HOME}"/dox/work/school/gsu/film/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsgh() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/school/gsu/history ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/history
		fi
		cd "${HOME}"/dox/work/school/gsu/history || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/school/gsu/history/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/history/"${1}"
		fi
		cd "${HOME}"/dox/work/school/gsu/history/"${1}" || return 1
		ls -a --color
	fi
}

cd_dsgm() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/school/gsu/misc ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/misc
		fi
		cd "${HOME}"/dox/work/school/gsu/misc || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/school/gsu/misc/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/school/gsu/misc/"${1}"
		fi
		cd "${HOME}"/dox/work/school/gsu/misc/"${1}" || return 1
		ls -a --color
	fi
}

cd_dvyt4() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound ]; then
			mkdir -p "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound
		fi
		cd "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound/"${1}" ]; then
			mkdir -p "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound/"${1}"
		fi
		cd "${HOME}"/dox/work/video-editing/youtube/404GenderNotFound/"${1}" || return 1
		ls -a --color
	fi
}

############################
### Pictures Directories ###
############################
cd_px() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/pix ]; then
			mkdir -p "${HOME}"/pix
		fi
		cd "${HOME}"/pix || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/pix/"${1}" ]; then
			mkdir -p "${HOME}"/pix/"${1}"
		fi
		cd "${HOME}"/pix/"${1}" || return 1
		ls -a --color
	fi
}

cd_pw() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/pix/wallpapers ]; then
			mkdir -p "${HOME}"/pix/wallpapers
		fi
		cd "${HOME}"/pix/wallpapers || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/pix/wallpapers/"${1}" ]; then
			mkdir -p "${HOME}"/pix/wallpapers/"${1}"
		fi
		cd "${HOME}"/pix/wallpapers/"${1}" || return 1
		ls -a --color
	fi
}

cd_psc() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/pix/screenshots ]; then
			mkdir -p "${HOME}"/pix/screenshots
		fi
		cd "${HOME}"/pix/screenshots || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/pix/screenshots/"${1}" ]; then
			mkdir -p "${HOME}"/pix/screenshots/"${1}"
		fi
		cd "${HOME}"/pix/screenshots/"${1}" || return 1
		ls -a --color
	fi
}

#################################
### Configuration Directories ###
#################################
cd_cfg() {
	if [ -z "${1}" ]; then
		if ! [ -d "${HOME}"/.config ]; then
			mkdir -p "${HOME}"/.config
		fi
		cd "${HOME}"/.config || return 1
		ls -a --color
	else
		if ! [ -d "${HOME}"/.config/"${1}" ]; then
			mkdir -p "${HOME}"/.config/"${1}"
		fi
		cd "${HOME}"/.config/"${1}" || return 1
		ls -a --color
	fi
}

###################
## Miscellaneous ##
###################
cheater() {
	curl -m 7 "https://cheat.sh/${1}"
}

check_alias() {
	alias | grep "${1}"
}

#################
## Set Aliases ##
#################
set_aliases() {
	# Git aliases.
	alias ga='git_add'
	alias gr='git_remove'
	alias grset='git_reset'
	alias gck='git_checkout'
	alias gpl='git_pull'
	alias gs='git status'
	alias gc='git commit -m'
	alias gf='git fetch'
	alias gp='git push'
	alias gcl='git clone'

	# Download directory aliases.
	alias dl='cd_dl'
	alias dff='cd_dff'
	alias dtc='cd_dtc'
	alias dlm='cd_dlm'

	# Document directory aliases.
	alias dx='cd_dx'
	alias ddfp='cd_ddfp'
	alias ddwp='cd_ddwp'
	alias ddxm='cd_ddxm'
	alias dddf='cd_dddf'
	alias ddx11='cd_ddx11'
	alias dn='cd_dn'
	alias dsg='cd_dsg'
	alias dsge='cd_dsge'
	alias dsgs='cd_dsgs'
	alias dsgf='cd_dsgf'
	alias dsgh='cd_dsgh'
	alias dsgm='cd_dsgm'
	alias dvyt4='cd_dvyt4'

	# Pix directory aliases.
	alias px='cd_px'
	alias pw='cd_pw'
	alias psc='cd_psc'

	# Configuration directory aliases.
	alias cfg='cd_cfg'

	# ls aliases.
	alias ls='ls --color'
	alias l='ls --color'
	alias la='ls -a --color'
	alias ll='ls -l --color'
	alias lt='ls -lt --color'

	# less aliases.
	alias less='less -R'

	# Miscellaneous aliases.
	alias failias='check_alias'
	alias cheat='cheater'
	alias c='clear'
	alias e='exit'
	alias v='emacs -nw'
}

##########
# Prompt #
##########
#####################
### Return Status ###
#####################
check_latest_return_status() {
	if [ "${return_status}" -eq 0 ]; then
		smiley="%F{199}${return_status}%f"
	else
		smiley="%F{196}${return_status}%f"
	fi

	printf "%s" "${smiley}"
}

###################
### Time Elapse ###
###################
typeset -F SECONDS

function record-start-time() {
	emulate -L zsh
	ZSH_START_TIME=${ZSH_START_TIME:-$SECONDS}
}

function report-start-time() {
	emulate -L zsh

	if [ "${ZSH_START_TIME}" ]; then
		DELTA=$(($SECONDS - $ZSH_START_TIME))
		DAYS=$((~~($DELTA / 86400)))
		HOURS=$((~~(($DELTA - $DAYS * 86400) / 3600)))
		MINUTES=$((~~(($DELTA - $DAYS * 86400 - $HOURS * 3600) / 60)))
		SECS=$(($DELTA - $DAYS * 86400 - $HOURS * 3600 - $MINUTES * 60))
		ELAPSED=''

		test "$DAYS" != '0' && ELAPSED="${DAYS}d"
		test "$HOURS" != '0' && ELAPSED="${ELAPSED}${HOURS}h"
		test "$MINUTES" != '0' && ELAPSED="${ELAPSED}${MINUTES}m"

		if [ "$ELAPSED" = '' ]; then
			SECS="$(print -f "%.2f" $SECS)s"
		elif [ "$DAYS" != '0' ]; then
			SECS=''
		else
			SECS="$((~~$SECS))s"
		fi

		ELAPSED="${ELAPSED}${SECS}"
		ITALIC_ON=$'\e[3m'
		ITALIC_OFF=$'\e[23m'
		export RPROMPT="%F{128}%{$ITALIC_ON%}${ELAPSED}%{$ITALIC_OFF%}%f"
		unset ZSH_START_TIME
	fi
}

##################
### Git Status ###
##################
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr "%F{128}●%f"
zstyle ':vcs_info:*' unstagedstr "%F{196}●%f"
zstyle ':vcs_info:*' use-simple true
zstyle ':vcs_info:git+set-message:*' hooks git-untracked
zstyle ':vcs_info:git*:*' formats '%F{199}-%f%F{128}[%f%F{199}%b%f%m%c%u%F{128}]%f'
zstyle ':vcs_info:git*:*' actionformats '%F{199}-%f%F{128}[%f%F{199}%b|%a%m%c%u%F{128}]%f'

function +vi-git-untracked() {
	emulate -L zsh

	if [[ -n $(git ls-files --exclude-standard --others 2> /dev/null) ]]; then
		hook_com[unstaged]+="%F{184}●%f"
	fi
}

###################
### Main Prompt ###
###################
prompt_init() {
	# Set the prompt.
	export PS1='%F{128}[%f`check_latest_return_status`%F{128}]%f${vcs_info_msg_0_} %F{199}>%f%F{190}>%f%F{51}>%f%F{184}%(1j.*.)%f '
}

#################
# Miscellaneous #
#################
#############
## Insults ##
#############
print_insult() {
	if [ "${return_status}" -eq 127 ]; then
		current_insult=$(sort -uR "${HOME}"/.local/share/lists/insults | head -n 1)
		current_insult="%F{196}${current_insult}%f"
		print -Pr "${current_insult}"
	fi
}

preexec() {
	# Set the elapsed time of the ran command.
	record-start-time
}

precmd() {
	# Capture return status.
	return_status="$?"

	# Load vcs_info.
	vcs_info

	# Report the start time of the last ran command.
	report-start-time

	# Use a beam cursor for new prompts.
	printf '\e[5 q'

	# If we got an error, insult the user.
	print_insult
}

print_quote() {
	current_quote=$(sort -uR "${HOME}"/.local/share/lists/quotes | head -n 1)
	print -Pr "%F{199}${current_quote}%f"
}

###################
## Miscellaneous ##
###################
compinit -d "${HOME}"/.cache/zsh/zcompdumb-"${ZSH_VERSION}"
setopt autocd autopushd pushdignoredups promptsubst correct histignorealldups histverify
ENABLE_CORRECTION="true"

###########
# Plugins #
###########
#########################
## Syntax Highlighting ##
#########################
plugins_load_syntaxhighlighting() {
	source "${HOME}"/.config/zsh/plugins/syntaxhighlighting/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh > /dev/null 2>&1
}

#################
## Suggestions ##
#################
plugins_load_suggestions() {
	source "${HOME}"/.config/zsh/plugins/suggestions/zsh-autosuggestions/zsh-autosuggestions.zsh > /dev/null 2>&1
}

########
# Main #
########
init_new_shell() {
	# Set the return status.
	return_status=0

	# Set history settings.
	set_history_settings

	# Set tab completion settings.
	set_tab_completion_method
	set_vi_tab_menu_navigation

	# Set line editing settings.
	set_vi_mode

	# Set modal cursor settings.
	zle -N zle-keymap-select
	zle -N zle-line-init

	# Set key bindings.
	set_keybindings

	# Set aliases.
	set_aliases

	# Set prompt.
	prompt_init

	# Print quote.
	print_quote

	# Load plugins.
	plugins_load_syntaxhighlighting
	plugins_load_suggestions
}

init_new_shell
