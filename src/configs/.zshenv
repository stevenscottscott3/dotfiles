# Direct zsh to use $HOME/.config/zsh (Can't be run from .zlogin for some reason).
ZDOTDIR="${HOME}"/.config/zsh

# Store coredumps in $HOME/.cache/zsh (Can't be run from .zlogin for some reason).
ZSH_COMPDUMP="${HOME}/.cache/zcompdump-${ZSH_VERSION}"
